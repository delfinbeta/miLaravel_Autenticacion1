<?php

use Faker\Generator as Faker;

$factory->define(App\Admin::class, function (Faker $faker) {
  static $password;

  return [
    'name' => $faker->name,
    'email' => $faker->unique()->safeEmail,
    'email_verified_at' => now(),
    'password' => $password ?: $password = bcrypt('clave123'),
    'remember_token' => str_random(10),
  ];
});
