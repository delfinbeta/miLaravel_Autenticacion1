<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminLogoutTest extends TestCase
{
	use RefreshDatabase;

  /** @test
   ** Comprobar que Usuario Admin cierra sesión */
  public function logout_admin()
  {
    auth('admin')->login($this->createAdmin());

    $this->assertAuthenticated('admin');

    $response = $this->post('admin/logout');

    $response->assertRedirect('/');

    $this->assertGuest('admin');
  }

  /** @test
   ** Comprobar que el cierre de sesión de un Admin no cierra la sesión de un User */
  public function logout_admin_no_termina_user()
  {
    auth('admin')->login($this->createAdmin());
    auth('web')->login($this->createUser());

    $adminSessionName = auth('admin')->getName();
    $webSessionName = auth('web')->getName();

    $this->assertAuthenticated('admin');
    $this->assertAuthenticated('web');

    $response = $this->post('admin/logout');

    $response->assertRedirect('/')
    	->assertSessionHas($webSessionName)
    	->assertSessionMissing($adminSessionName);

    $this->assertGuest('admin');
    $this->assertAuthenticated('web');
  }
}
