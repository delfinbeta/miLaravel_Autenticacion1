<?php

namespace Tests\Feature\Admin\Usuarios;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminUsuariosTest extends TestCase
{
  use RefreshDatabase;

  /** @test
   ** Comprobar que Usuario Admin inicia sesión correctamente */
  public function login_usuario_admin()
  {
    $userAdmin = $this->createAdmin();

    $this->actingAsAdmin($userAdmin)
    		 ->get(route('admin_usuarios'))
    		 ->assertStatus(200)
    		 ->assertSee('Admin - Usuarios');
  }

  /** @test
   ** Comprobar que Usuario no Admin es Redirigido */
  public function login_otro_usuario()
  {
    $user = $this->createUser();

    $this->actingAsUser($user)
    		 ->get(route('admin_usuarios'))
    		 ->assertStatus(302)
         ->assertRedirect('admin/login');
  }

  /** @test
   ** Comprobar que Usuario es Redirigido si no inicia sesión correctamente */
  public function login_no_ejecutado()
  {
    $this->get(route('admin_usuarios'))
    		 ->assertStatus(302)
    		 ->assertRedirect('admin/login');
  }
}
