<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminLoginTest extends TestCase
{
  use RefreshDatabase;

  /** @test
   ** Comprobar que Usuario Admin inicia sesión correctamente */
  public function login_admin()
  {
    $email = 'dkbetancourt@gmail.com';
    $password = 'clave123';

    $admin = $this->createAdmin([
    	'email' => $email,
    	'password' => bcrypt($password)
    ]);

    $this->post('admin/login', compact('email', 'password'))
    		 ->assertRedirect('admin');

    $this->assertAuthenticatedAs($admin, 'admin');
  }

  /** @test
   ** Comprobar que Usuario con credenciales invalidas no puede iniciar sesión en admin*/
  public function login_admin_incorrecto()
  {
    $email = 'dkbetancourt@gmail.com';
    $password = 'clave123';

    $admin = $this->createAdmin([
    	'email' => $email,
    	'password' => bcrypt($password)
    ]);

    $this->post('admin/login', ['email' => $email, 'password' => 'clavemala'])
    		 ->assertStatus(302)
    		 ->assertSessionHasErrors();

    $this->assertGuest();
  }

  /** @test
   ** Comprobar que Usuario tipo User no puede iniciar sesión en admin*/
  public function user_no_login_admin()
  {
    $email = 'dkbetancourt@gmail.com';
    $password = 'clave123';

    $admin = $this->createUser([
    	'email' => $email,
    	'password' => bcrypt($password)
    ]);

    $this->post('admin/login', compact('email', 'password'))
    		 ->assertStatus(302)
    		 ->assertSessionHasErrors();

    $this->assertGuest();
  }
}
