<?php

namespace Tests\Feature\Admin;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OcultarRutasAdminTest extends TestCase
{
  use RefreshDatabase;

  /** @test
   ** Redirigir URLs no validas al login */
  public function redirigir_urls_invalidas()
  {
    $this->get('admin/invalid-url')
    		 ->assertStatus(302)
    		 ->assertRedirect('admin/login');
  }

  /** @test
   ** Mostrar error 404 a usuarios autenticados que llamaron URLs no validas */
  public function error404_urls_invalidas_admin()
  {
  	$userAdmin = $this->createAdmin();

    $this->actingAsAdmin($userAdmin)
    		 ->get('admin/invalid-url')
    		 ->assertStatus(404);
  }

  /** @test
   ** Redirigir URLs no validas POST al login */
  public function redirigir_urls_invalidas_post()
  {
    $this->post('admin/invalid-url')
    		 ->assertStatus(302)
    		 ->assertRedirect('admin/login');
  }
}
