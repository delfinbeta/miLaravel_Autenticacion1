<?php

namespace Tests\Feature\Admin;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DashboardTest extends TestCase
{
	use RefreshDatabase;

  /** @test
   ** Comprobar que Usuario Admin inicia sesión correctamente */
  public function login_usuario_admin()
  {
    $userAdmin = $this->createAdmin();

    $this->actingAsAdmin($userAdmin)
    		 ->get(route('admin'))
    		 ->assertStatus(200)
    		 ->assertSee('Dashboard');
  }

  /** @test
   ** Comprobar que Usuario no Admin es Redirigido */
  public function login_otro_usuario()
  {
    $user = $this->createUser();

    $this->actingAsUser($user)
    		 ->get(route('admin'))
         ->assertStatus(302)
         ->assertRedirect('admin/login');
  }

  /** @test
   ** Comprobar que Usuario es Redirigido si no inicia sesión correctamente */
  public function login_no_ejecutado()
  {
    $this->get(route('admin'))
    		 ->assertStatus(302)
    		 ->assertRedirect('admin/login');
  }

  /** @test
   ** Mostrar página de cambio de password a usuario correctamente autenticado */
  public function mostrar_cambio_password()
  {
    $user = $this->createUser();

    $this->actingAsUser($user)
         ->get(route('password'))
         ->assertStatus(200)
         ->assertSee('Cambia tu Contraseña');
  }

  /** @test
   ** Redirigir al usuario no autenticado desde el cambio de password al login */
  public function redirigir_cambio_password()
  {
    $this->get(route('password'))
    		 ->assertStatus(302)
    		 ->assertRedirect('admin/login');
  }
}
