<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Blade;

class CustomDirectivesTest extends TestCase
{
  use RefreshDatabase;

  /** @test
   ** Comprobar Directiva admin */
  public function directiva_admin()
  {
    $this->assertFalse(Blade::check('admin'));
  }
}
