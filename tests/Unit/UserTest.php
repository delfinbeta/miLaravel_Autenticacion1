<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
  use RefreshDatabase;

  /** @test
   ** Comprobar que Usuario puede ser Admin */
  public function usuario_admin()
  {
    // $user = factory(User::class)->create(['admin' => false]);

    // $user->refresh();

    // $this->assertFalse($user->isAdmin());

    // $user->admin = true;
    // $user->save();

    // $this->assertTrue($user->isAdmin());

    $user = factory(User::class)->create();
    $user->refresh();
    $this->assertFalse($user->isAdmin());
  }
}
