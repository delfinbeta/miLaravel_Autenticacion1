<?php

// use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group.
|
*/

Route::get('/', 'AdminController@index')->name('admin');

Route::get('/usuarios', function() {
	return "Admin - Usuarios";
})->name('admin_usuarios');

Route::get('/noticias', function() {
	return "Admin - Noticias";
})->name('admin_noticias');

Route::catch(function() {
	return response()->view('errors.404', [], 404);

	// throw new NotFoundHttpException();
});
